const db = require("../models");
const Mobile = db.mobiles;
const Op = db.Sequelize.Op;


exports.create = (req, res) => {
    // Validate request
    if (!req.body.Mobile_Name) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Tutorial
    const mobile = {
     Mobile_Name : req.body.Mobile_Name,
     Price: req.body.Price,
     RAM: req.body.RAM,
     Internal_Storage:req.body.Internal_Storage

    };
  
    // Save Tutorial in the database
    Mobile.create(mobile)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the mobile."
        });
      });
  };


  exports.findAll = (req, res) => {
    // const title = req.query.title;
    // console.log('titlle',title)
    // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    // console.log(condition)
  
    Mobile.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Mobiles."
        });
      });
  };

  


  exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Mobile.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Mobile with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Mobile with id=" + id
        });
      });
  };




  exports.update = (req, res) => {
    const id = req.params.id;
  
    Mobile.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        // console.log(num
        if (num == 1) {
          res.send({
            message: "Mobile was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Mobile with id=${id}. Maybe Mobile was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Mobile with id=" + id
        });
      });
  };


  exports.delete=(req,res)=>{
    const id=req.params.id;
    Mobile.destroy({where:{id:id}})
    .then(data=>{
      if(data==1){
        res.send({
          message:`Mobile deleted succesfully with id: ${id}`
        })
      } 
      else{
        res.send({
          message:`Cannot Delete Mobile with id: ${id} may be Mobile was not found`
        })
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Tutorial with id=" + id
      });
    });

  }

exports.findAllWithCond=(req,res)=>{
  Mobile.findAll({where :{
    Price:{
      [Op.lt]: 50000
      }
    }
  })
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving Mobiles."
    });
  });
}