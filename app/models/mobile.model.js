module.exports = (sequelize, Sequelize) => {
    const Mobile = sequelize.define("mobile", {
      Mobile_Name: {
        type: Sequelize.STRING,
        
      },
      Price: {
        type: Sequelize.INTEGER,
    
      },
      RAM: {
        type: Sequelize.STRING,
          
      },
      
       Internal_Storage:
       {
           type:Sequelize.STRING  
       }
      
    }, {
      timestamps:false
    });
  
    return Mobile;
  };
  