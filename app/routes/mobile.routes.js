
module.exports = app => {
    const mobiles = require("../controllers/mobile.controller.js");
  
    var router = require("express").Router();
  
    // Create a new mobile
    router.post("/", mobiles.create);
  
    // Retrieve all mobiles
    router.get("/", mobiles.findAll);

    router.get('/condition',mobiles.findAllWithCond)
  
    // Retrieve a single MOBILE with id
    router.get("/:id", mobiles.findOne);
  
    // Update a mobile with id
    router.put("/:id", mobiles.update);
  
    // Delete a Mobile with id
    router.delete("/:id", mobiles.delete);
  
  
    app.use('/api/mobiles', router);
  };
  